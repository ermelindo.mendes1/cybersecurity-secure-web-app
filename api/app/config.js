require('dotenv').config();

module.exports = {
  development: {
    type: 'development',
    // port: 3000,
    // mongodb: 'mongodb+srv://brunel:DnJJkrtZWI61L95p@cluster0.dncjrhq.mongodb.net/?retryWrites=true&w=majority'
    port: process.env.PORT,
    mongodb: process.env.DATABASE_URL
  },
  production: {
    type: 'production',
    port: process.env.PORT,
    mongodb: process.env.DATABASE_URL
    // port: 3000,
    // mongodb: process.env.DATABASE_URL
  }
  
}