const UserModel = require('../models/user.js')
const bcrypt = require('bcrypt');
// const { body, validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');
// const expressJwt = require('express-jwt');
const secretKey = "teste"
const Login = class Login {
  /**
   * @constructor
   * @param {Object} app
   * @param {Object} config
   */
  constructor (app, connect) {
    this.app = app
    this.UserModel = connect.model('User', UserModel)
    // this.secretKey = process.env.TOKEN_KEY

    this.run()
  }

  auth() {
    this.app.get('/auth/', (req, res) => {
      try {
        res.status(200).json({ 'message': 'ok' })
      } catch (err) {
        console.error(`[ERROR] POST logins/ -> ${err}`)
  
        res.status(500).json({
          code: 500,
          message: 'Internal server error'
        })
      }
    })
  }

  // auth(req, res) {
  //   if (req.session.user) {
  //     res.status(200).json({ code: 200, message: 'User already authenticated' });
  //   } else {
  //     res.status(401).json({ code: 401, message: 'User not authenticated' });
  //   }
  // }
  
  getByLoginPassword() {
    this.app.post('/login/',  async (req, res) => {

      try {
        const { name, password } = req.body;
  
        const user = await this.UserModel.findOne({ name: name });
        // console.log(user);
        if (!user) {
          // L'utilisateur n'existe pas
          return res.status(401).json({ code: 401, message: 'Invalid credentials' });
        }
  
        // const isPasswordValid = await bcrypt.compare(password, user.password);
        // if (!isPasswordValid) {
        //   return res.status(401).json({ code: 401, message: 'Invalid credentials' });
          
        // }

        // A utiliser pour tester car sur la base pas de mot de passe crypter ne pas oublié de commanter l'autre condition et bcrypt
        if (user.password !== password) {
          console.log('mauvais mot de passe');
          return res.status(401).json({ code: 401, message: 'Invalid credentials' });
        }
        if (user.password === password || user.name === name) {
          const token = jwt.sign({ name }, secretKey, { expiresIn: '1h' });
          // const token = jwt.sign({ name }, this.secretKey, { expiresIn: '1h' });
          res.cookie('token', token, { httpOnly: true, secure: false, sameSite: 'Strict', domain: 'localhost', path: '/' });
          res.status(200).json({ token });
          // res.json({ token });
          // res.status(200).json({ code: 200, message: 'Authentication successful' }); //,redirect: '/admin',
          
        }
      } catch (err) {
        console.error(`[ERROR] POST logins/ -> ${err}`);
        res.status(500).json({
          code: 500,
          message: 'Internal server error',
        });
      }
    });
  }
  

  // getByLoginPassword () {
  //   this.app.get('/login/', (req, res) => {
  //     try {
  //       res.status(200).json({ code: 200, message: 'ok' });
  //     } catch (err) {
  //       console.error(`[ERROR] POST logins/ -> ${err}`)
  
  //       res.status(500).json({
  //         code: 500,
  //         message: 'Internal server error'
  //       })
  //     }
  //   })
  // }

  /**
   * Run
   */
  run () {
    this.auth()
    this.getByLoginPassword()
  }
}

module.exports = Login
